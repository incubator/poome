# Poome

Poome stands for "Persistence of objects made easy".

Poome is a JAVA library that gives you the opportunity to use CQRS pattern
with EventStore for write and and Graphql for query.

You can find more information on the [website](http://poome.nuiton.org/);
