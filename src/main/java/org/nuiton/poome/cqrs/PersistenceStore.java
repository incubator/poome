package org.nuiton.poome.cqrs;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 */
public interface PersistenceStore {
    <A> void store(Class<A> type, String id, Event<A> e) throws IOException;
    <A> List<Event<A>> read(Date checkoutDate, Class<A> type, String id) throws IOException;
    <A> Stream<String> getIds(Date checkoutDate, Class<A> aClass) throws IOException;
}
