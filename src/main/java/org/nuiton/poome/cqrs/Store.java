package org.nuiton.poome.cqrs;

import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.map.LazyMap;
import org.nuiton.poome.cqrs.annotation.EventListener;
import org.nuiton.poome.cqrs.annotation.EventVetoable;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

/**
 *
 */
public class Store {

    @RequiredArgsConstructor
    private static class BindMethod {
        private final Object o;
        private final Method m;
        public Object call(Object...args) throws RuntimeException {
            try {
                return m.invoke(o, args);
            } catch (Exception eee) {
                throw new RuntimeException(eee);
            }
        }
    }

    private final PersistenceStore persistenceStore;

    private final Store parent;
    private final Date checkoutDate;

    private final Map<Class, List<BindMethod>> handlers;
    private final Map<Class, List<BindMethod>> vetoables;

    private final Map<String, Object> cache = new WeakHashMap<>();
    private final ExecutorService executor;

    public Store(PersistenceStore persistenceStore) {
        executor = Executors.newCachedThreadPool();
        handlers = LazyMap.lazyMap(new HashMap<Class, List<BindMethod>>(), () -> new LinkedList<>());
        vetoables = LazyMap.lazyMap(new HashMap<Class, List<BindMethod>>(), () -> new LinkedList<>());
        this.persistenceStore = persistenceStore;
        this.parent = null;
        this.checkoutDate = null;
    }

    protected Store(Store parent, Date checkoutDate) {
        executor = Executors.newCachedThreadPool();
        handlers = LazyMap.lazyMap(new HashMap<Class, List<BindMethod>>(), () -> new LinkedList<>());
        vetoables = LazyMap.lazyMap(new HashMap<Class, List<BindMethod>>(), () -> new LinkedList<>());
        this.parent = parent;
        this.checkoutDate = checkoutDate;
        this.persistenceStore = null;
    }

    public boolean isCheckout() {
        return checkoutDate != null;
    }

    public Store checkout(Date d) {
        return new Store(this, d);
    }

    public <A> EventStore<A> getEventStore(Class<A> a) {
        return new EventStore<>(this, a);
    }

    public void addListener(Object l) {
        Method[] methods = l.getClass().getMethods();
        for (Method m : methods) {
            register(handlers, l, m, EventListener.class);
            register(vetoables, l, m, EventVetoable.class);
        }
    }

    private void register(Map<Class, List<BindMethod>> map, Object l, Method m, Class c) {
        Object aEvent = m.getAnnotation(c);
        if (aEvent != null) {
            Class[] types = m.getParameterTypes();
            if (types.length == 1) {
                map.get(types[0]).add(new BindMethod(l, m));
            } else {
                throw new RuntimeException(
                        "Bad event listener method, your method must have only one argument (Event)" + m);
            }
        }
    }

    private PersistenceStore getPersistenceStore() {
        if (parent != null) {
            return parent.getPersistenceStore();
        }
        return persistenceStore;
    }

    // package visibility method (need for EventStore
    void addEvent(Class aClass, Event e) {
        if (!isCheckout()) {
            try {
                callVetoable(vetoables, e);
                getPersistenceStore().store(aClass, e.getRootId(), e);
                removeFromCache(e.getRootId());
                callListener(handlers, e);
            } catch(Exception eee) {
                throw new RuntimeException("Event not stored", eee);
            }
        } else {
            throw new RuntimeException("Add event is disallow on checkout");
        }
    }

    <A> List<Event<A>> getEvents(Class<A> aClass, String id) {
        if (id == null) {
            return Collections.EMPTY_LIST;
        }
        try {
            return getPersistenceStore().read(checkoutDate, aClass, id);
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    <A> A get(Class<A> aClass, String id) {
        A result = getFromCache(aClass, id);
        if (result == null) {
            List<Event<A>> events = getEvents(aClass, id);
            for (Event<A> e : events) {
                result = e.apply(this, result);
                addToCache(id, result);
            }
        }
        return result;
    }

    <A> Stream<String> getIds(Class<A> aClass) {
        try {
            return getPersistenceStore().getIds(checkoutDate, aClass);
        } catch (IOException eee) {
            throw new RuntimeException(eee);
        }
    }

    private void addToCache(String id, Object o) {
        cache.put(id, o);
    }

    private void removeFromCache(String id) {
        cache.remove(id);
    }
    private <A> A getFromCache(Class<A> aClass, String id) {
        Object result = cache.get(id);
        return aClass.cast(result);
    }

    private void callVetoable(Map<Class, List<BindMethod>> map, Event e) throws Exception {
        List<Class> classes = getHierarchyClass(e);
        for (Class c : classes) {
            map.get(c).stream().forEach(b -> b.call(e));
        }
    }


    private void callListener(Map<Class, List<BindMethod>> map, Event e) {
        List<Class> classes = getHierarchyClass(e);
        for (Class c : classes) {
            map.get(c).forEach(b -> executor.submit((Runnable) () -> b.call(e)));
        }
    }

    private List<Class> getHierarchyClass(Object o) {
        List<Class> result = new LinkedList<>();
        Class c = o.getClass();
        result.add(c);
        while (c != null) {
            c = c.getSuperclass();
            result.add(c);
        }
        return result;
    }
}
