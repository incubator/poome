package org.nuiton.poome.cqrs;

import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 */
public class EventStore<A> {

    private final Store store;
    private final Class<A> aClass;

    protected EventStore(Store store, Class<A> aClass) {
        this.store = store;
        this.aClass = aClass;
    }

    public EventStore<A> checkout(Date d) {
        return store.checkout(d).getEventStore(aClass);
    }

    public void addEvent(Event<A> e) {
        store.addEvent(aClass, e);
    }

    public List<Event<A>> getEvents(String id) {
        return store.getEvents(aClass, id);
    }

    public A get(String id) {
        return store.get(aClass, id);
    }

    public Stream<String> getIds() {
        return store.getIds(aClass);
    }
}
