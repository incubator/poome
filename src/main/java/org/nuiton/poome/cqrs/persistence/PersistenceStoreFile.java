package org.nuiton.poome.cqrs.persistence;

import io.protostuff.LinkedBuffer;
import io.protostuff.ProtostuffIOUtil;
import io.protostuff.Schema;
import io.protostuff.runtime.RuntimeSchema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.nuiton.poome.cqrs.Event;

import org.nuiton.poome.cqrs.EventErase;
import org.nuiton.poome.cqrs.PersistenceStore;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.READ;

public class PersistenceStoreFile implements PersistenceStore {

    private final Path directory;
    private final Schema schema;

    public PersistenceStoreFile(Path directory) throws IOException {
        Files.createDirectories(directory);
        this.directory = directory;
        schema = RuntimeSchema.getSchema(Item.class);
    }

    @Override
    public <A> void store(Class<A> type, String id, Event<A> e) throws IOException {
        Path file = directory.resolve(type.getName()).resolve(id);
        Files.createDirectories(file.getParent());
        LinkedBuffer buffer = LinkedBuffer.allocate();
        try (OutputStream out = new BufferedOutputStream(
                Files.newOutputStream(file, CREATE, APPEND))) {
            ProtostuffIOUtil.writeDelimitedTo(out, new Item(e), schema, buffer);
        } finally {
            buffer.clear();
        }
        if (e instanceof EventErase) {
            Path deleteDir = directory.resolve(type.getName()).resolve("deleted");
            Files.createDirectories(deleteDir);
            Path dest = directory.resolve(type.getName()).resolve("deleted").resolve(id);
            Files.move(file, dest);
            Path linkDest = FileSystems.getDefault().getPath("deleted", id);
            Files.createSymbolicLink(file, linkDest);
        }
    }

    @Override
    public <A> List<Event<A>> read(Date checkoutDate, Class<A> type, String id) throws IOException {
        List<Event<A>> result = new ArrayList<>();
        Path file = directory.resolve(type.getName()).resolve(id);
        if (Files.exists(file)) {
            long size = Files.size(file);
            long read = 0;
            try (BufferedInputStream in = new BufferedInputStream(
                    Files.newInputStream(file, READ))) {
                Item i = (Item) schema.newMessage();
                while (read < size) {
                    read += 2 + ProtostuffIOUtil.mergeDelimitedFrom(in, i, schema);
                    if (checkoutDate != null && i.getE().getEventDate().after(checkoutDate)) {
                        break;
                    }
                    result.add(i.getE());
                }
            }
        }
        return result;
    }

    @Override
    public <A> Stream<String> getIds(Date checkoutDate, Class<A> aClass) throws IOException {
        Path file = directory.resolve(aClass.getName());
        return Files.find(file, 1, (p, a) -> {
            return a.isRegularFile() && (checkoutDate == null || a.creationTime().toMillis() <= checkoutDate.getTime());
        }).map(p -> p.getFileName().toString());
    }


    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Item {
        Event e;
    }
}
