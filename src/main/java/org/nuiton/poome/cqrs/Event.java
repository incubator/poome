package org.nuiton.poome.cqrs;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

/**
 *
 */
@Getter
@Setter
@AllArgsConstructor
public abstract class Event<A> {

    private String eventId;
    private Date eventDate;

    public Event() {
        eventId = UUID.randomUUID().toString();
        eventDate = new Date();
    }

    abstract public String getRootId();
    abstract public A apply(Store store, A a);

}
