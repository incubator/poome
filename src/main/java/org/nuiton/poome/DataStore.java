package org.nuiton.poome;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.spgeed.annotations.Select;
import org.nuiton.spgeed.annotations.Update;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class DataStore {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(DataStore.class);

    private static final String ID = "@id";
    private static final String TYPE = "@type";
    private static final String ID_REF = "@idRef";

    private String idFieldName;
    private ObjectMapper mapper;

    public DataStore(String idFieldName) {
        this.idFieldName = idFieldName;

        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS); // store date as number
//        mapper.enableDefaultTyping();
    }

    public Object read(String id) {
        Object result = readObject(id, new HashMap<>());
        return result;
    }

    private Object readObject(String id, Map<String, Object> done) {
        try {
            Object result = done.get(id);
            if (result == null) {
                ObjectRow row = readOneObject(id);
                System.out.println("read row:" + row);
                Map map = jsonToMap(row.getJson());
                if (row.getIdref() != null) {
                    for (String childId : row.getIdref()) {
                        readObject(childId, done);
                    }
                }
                List<String> idref = (List<String>)map.get(ID_REF);
                if (idref != null) {
                    for (String name : idref) {
                        String uid = (String)map.get(name);
                        Object o = done.get(uid);
                        map.put(name, o);
                    }
                }
                Class c = Class.forName(row.getType());
                System.out.println("map: " + map + " to: " + c);
                result = mapper.convertValue(map, c);
                done.put(id, result);
            }
            return result;
        } catch(Exception eee) {
            throw new RuntimeException("Can't read object", eee);
        }

    }

    public void add(Object o) {
        try {
            if (o == null) {
                return;
            }

            String id = getId(o);
            if (StringUtils.isBlank(id)) {
                throw new IllegalArgumentException(String.format(
                        "Your object (%s) doesn't have id (%s)", o, idFieldName));
            }
            List<Map<String, Object>> splitObject = splitObject(o);
            System.out.println("split result: " + splitObject);

            List<ObjectRow> rows = splitObject.stream()
                    .map(e -> {
                        String eid = (String)e.get(ID);
                        String type = (String)e.get(TYPE);
                        String[] ref = (String[])e.get(ID_REF);
                        String json = toJson(e);
                        if (ref != null) {
                            Arrays.stream(ref).map(name -> e.get(name)).collect(Collectors.toList()).toArray(ref);
                        }
                        return new ObjectRow(eid, type, ref, json);
                    })
                    .collect(Collectors.toList());
            System.out.println("json result: " + rows);

            store(rows);

        } catch(Exception eee) {
            throw new RuntimeException("Can't store object", eee);
        }
    }

    private String getId(Object o) {
        String id = null;
        try {
            id = FieldUtils.readField(o, idFieldName, true).toString();
        } catch (Exception eee) {
            // can't access only log
            log.debug("can't access to field:" + idFieldName);
        }
        return id;
    }

    private List<Map<String, Object>> splitObject(Object o) {
        List<Map<String, Object>> result = new ArrayList<>();

        Set<String> done = new HashSet();
        Queue todo = new LinkedList();
        todo.add(o);


        Object current;
        while ((current = todo.poll()) != null) {
            String currentId = getId(current);
            if (done.add(currentId)) { // if already in done, do nothing
                Map<String, Object> map = objectToMap(current);
                map.put(ID, currentId);
                map.put(TYPE, current.getClass().getName());

                List<String> idRef = new LinkedList<>();
                map.entrySet().stream().forEach(e -> {
                    Object value = e.getValue();
                    String id = getId(value);
                    if (StringUtils.isNotBlank(id)) {
                        todo.add(value);
                        e.setValue(id);
                        idRef.add(e.getKey());
                    }
                });

                if (!idRef.isEmpty()) {
                    map.put(ID_REF, idRef.toArray(new String[idRef.size()]));
                }

                result.add(map);
            }
        }

        return result;
    }

    private Map<String, Object> jsonToMap(String json) throws IOException {
        return mapper.readValue(json, Map.class);
    }

    private String toJson(Object o) {
        try {
            String result = mapper.writeValueAsString(o);
            return result;
        } catch (Exception eee) {
            throw new RuntimeException("Can't convert object to json", eee);
        }
    }

    private Map<String, Object> objectToMap(Object o) {
        try {
            Map<String, Object> result = new LinkedHashMap<>();//mapper.convertValue(o, Map.class);
            BeanInfo info = Introspector.getBeanInfo(o.getClass());
            for (PropertyDescriptor p : info.getPropertyDescriptors()) {
                String name = p.getName();
                Method getter = p.getReadMethod();
                if (getter != null && !"class".equals(name)) {
                    result.put(name, getter.invoke(o));
                }
            }
            return result;
        } catch (Exception eee) {
            throw new RuntimeException("Can't transform object (" + o.getClass() + ") to map", eee);
        }
    }

    @Update(sql = "CREATE TABLE IF NOT EXISTS objects (id TEXT PRIMARY KEY, type TEXT NOT NULL, idref TEXT[], json JSONB NOT NULL);"
            + "CREATE INDEX objectsgin ON objects USING GIN (json);")
    abstract protected void createdb();

    @Update(sql = "INSERT INTO objects (id, type, idref, json) VALUES ${data |values('id', 'type', 'idref', 'json::jsonb')}"
            + " ON CONFLICT (id) DO UPDATE SET json = EXCLUDED.json, idref = EXCLUDED.idref")
    abstract protected void store(List<ObjectRow> data);

    @Select(sql = "SELECT id, type, idref, json::text FROM objects WHERE id=${id}")
    abstract protected ObjectRow readOneObject(String id);

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    private static class ObjectRow {
        private String id;
        private String type;
        private String[] idref;
        private String json;
    }

}
