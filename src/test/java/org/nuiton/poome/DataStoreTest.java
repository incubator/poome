package org.nuiton.poome;

import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.SingleInstancePostgresRule;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.nuiton.poome.DataStore;
import org.nuiton.spgeed.SqlSession;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

public class DataStoreTest {

    @Rule
    public SingleInstancePostgresRule database = EmbeddedPostgresRules.singleInstance();

    @Test
    public void store() throws SQLException {
        A a = new A();
        a.setDate(new Date());
        a.setUid(UUID.randomUUID().toString());

        B b = new B();
        b.setDate(new Date());
        b.setUid(UUID.randomUUID().toString());
        b.setS("hello");

        a.setB(b);

        DataSource ds = database.getEmbeddedPostgres().getPostgresDatabase();

        try (SqlSession session = new SqlSession(ds)) {
            DataStore dataStore = session.getDao(DataStore.class, "uid");
            dataStore.createdb();
            dataStore.add(a);
            A r = (A) dataStore.read(a.getUid());
            System.out.println("read:" + r);
            Assert.assertEquals(a, r);
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode
    static class A {
        private String uid;
        private Date date;
        private B b;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString(callSuper = true)
    @EqualsAndHashCode(callSuper = true)
    static class B extends A {
        private String s;
    }
}
