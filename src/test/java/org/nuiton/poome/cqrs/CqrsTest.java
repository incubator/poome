package org.nuiton.poome.cqrs;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.junit.Test;
import org.nuiton.poome.cqrs.persistence.PersistenceStoreFile;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.UUID;
import java.util.stream.Collectors;

public class CqrsTest {

    @Getter
    @Setter
    @ToString
    public static class Person {
        private String uid;
        private String title; // Dr., M., Mme., ...
        private String firstName;
        private String lastName;
        private String diploma;
    }

    @Getter
    @Setter
    @ToString
    public static class PersonCreate extends Event<Person> {
        private String uid;
        private String title;
        private String firstName;
        private String lastName;
        private String diploma;

        @Override
        public String getRootId() {
            return uid;
        }

        @Override
        public Person apply(Store store, Person person) {
            Person result = new Person();
            result.setUid(uid);
            result.setTitle(title);
            result.setFirstName(firstName);
            result.setLastName(lastName);
            result.setDiploma(diploma);

            return result;
        }

    }

    @Test
    public void test() throws Exception {
        Path path = FileSystems.getDefault().getPath("target", "tmp", "cqrs-data");
        Store store = new Store(new PersistenceStoreFile(path));

        PersonCreate pc = new PersonCreate();
        pc.setUid(UUID.randomUUID().toString());
        pc.setTitle("M.");
        pc.setFirstName("Benjamin");
        pc.setLastName("Poussin");
        pc.setDiploma("DESS");

        EventStore<Person> esPerson = store.getEventStore(Person.class);
        esPerson.addEvent(pc);


        Person p = esPerson.get(pc.getUid());
        System.out.println("person: " + p);

        System.out.println("ids: " + esPerson.getIds().collect(Collectors.toList()));
    }
}
